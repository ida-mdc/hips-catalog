---
layout: page
title: What are HIP Solutions
permalink: /docs/overall/what-are-hips/
---

HIP Solutions are developed as a part of the Helmholtz Imaging
Platform's (HIP) mission to bring scientists and engineers in the
Helmholtz Association together to promote and develop imaging science
and to foster synergies across imaging modalities and applications
within the Helmholtz Association.

HIP Solutions (HIPS) are algorithms, tutorials, and data that are
provided by the HIPS framework. HIPS are based upon FAIR data
principles (Wilkinson et al, 2016) of Findability, Accessibility,
Interoperability, and Reproducibility. HIPS are designed to work with
a variety of software tools, plugins, and scripts to ensure adherence to
FAIR data standards. Potential HIPS developers are directed to these guidelines 
to ensure that their software/framework/toolkits adhere to [coding
standards]({{ '/docs/maintainer/coding-standards/' | prepend: site.baseurl | prepend: site.url }}). The specific HIPS
implementations are expected to follow specific [contribution and
maintenance guidelines]({{ '/docs/maintainer/contributing-hips/' | prepend: site.baseurl | prepend: site.url }}).


HIPS have been designed to address the discrepancy between software
developed as research and software that is designed to be used for
research. HIPS have permanence, are strictly versioned, are
cross-platform whenever possible, are indexed/searchable, and are citable,
including their origin software implementation.

The HIPS framework provides a way of accessing and using HIPS that
provides cross-platform implementations, tutorial interfaces,
containerization, graphical interfaces, and more.


**References**

Wilkinson, Mark D.; Dumontier, Michel; Aalbersberg, IJsbrand Jan; Appleton, Gabrielle; et al. (15 March 2016). "The FAIR Guiding Principles for scientific data management and stewardship". Scientific Data. 3: 160018. doi:10.1038/sdata.2016.18. OCLC 961158301. PMC 4792175. PMID 26978244
