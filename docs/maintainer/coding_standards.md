---
layout: page
title: Coding standards
permalink: /docs/maintainer/coding-standards/
---

Coding standards are important for ensuring the quality and
maintainability of software that is provided as HIP Solutions.

# Language neutral coding standards

1. Use consistent indenting. Each scope/nesting should be on a new
   indentation level. Tabs v. spaces is an endless debate, but display
   behavior in editors is most consistently defined for spaces.
2. Variable names should be meaningful and short. Acronyms are not a
   good choice. HIPS code will be read by experts from other domains
   and fields who may not be familiar with acroynms from your field.
3. Comments are important. It is not sufficient to assume that code is
   self documenting. The behavior of functions and methods should
   always be documented, including descriptions of inputs and
   outputs. Complex code fragments should be explained in natural
   language.
4. Usage of programs should be documented. Examples of command line usage or
   grapical interface usage instructions should be be provided along
   with code.
5. Hard coded values, "magic numbers" should be avoided. These are
   specific numerical values that are embedded directly within
   code. These values should be soft coded as best as possible,
   e.g. replaced by `#define` or variables, and these instances should
   be clearly identified.
6. Consistency should be maintained when possible. Variables that are
   consistently used in the same situation should use the same name
   (e.g. `index` in the case of a loop iterator).
7. Use naming conventions for variables and function names, and use
   these conventions consistently. 
8. Familiarize yourself with the common coding standards of your
   language of choice:
   - [Python](https://www.python.org/dev/peps/pep-0008/)
   - [C++](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
   - [Java](https://google.github.io/styleguide/javaguide.html)
9. Use [unit tests](https://en.wikipedia.org/wiki/Unit_testing) and [integration tests](https://en.wikipedia.org/wiki/Integration_testing) whenever possible. 
   
## Additional resources

- [Language neutral coding style on
Wikibooks](https://en.wikibooks.org/wiki/Computer_Programming/Coding_Style)
- [Language specific style guides from Google](https://google.github.io/styleguide/)

