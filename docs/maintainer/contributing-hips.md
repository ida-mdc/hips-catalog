---
layout: page
title: Contributing HIPS
permalink: /docs/maintainer/contributing-hips/
---

The HIPS framework provide a way of delivering algorithms,
tutorials, and data to users as HIP Solutions. 

# How to make a HIPS?

A HIPS can be created by starting from a skeleton project which can be
created by calling `hips init my_solution.py`. This will initialize
the skeleton of a HIPS inside the `my_solution.py` file. You can then
open this file in your favorite editor and fill out the necessary details.

# How to deploy a HIPS?

Once you have created your HIPS you can deploy a hips by running the
`hips deploy` command. This will create and submit a merge request to
the HIPS catalog which will then be reviewed by the HIP team. 

# How to maintain a HIPS?

In addition to following the guidelines for contributing a HIPS,
ensure that your version of `hips` is up to date.

# Requirements

Contributed HIPS must adhere to a set of standards in order to become
accepted into core HIPS catalog. 

## Fixed Versions

HIPS must use fixed versioning to ensure reproducibility. 

## Coding style

HIPS are defined in Python and should adhere to standard Python coding
standards, including [pep8](https://www.python.org/dev/peps/pep-0008/)
formatting.

Additional recommendations for HIPS are:

- use the `hips` logger
- initalization and runtime code should be encapsulated in the
  originating repository

# Specification

A HIPS file is a Python file that has *at least* the following
contents, and a set of keys and values within the `hips.setup` call.

```python

from hips.core import setup

setup(
    ...
)
```

where the HIPS metadata can be supplied to the `hips.setup` call as
keyword arguments.

## HIPS Metadata

`name` is the name of your solution.

`group` is your organization or username associated with the HIPS.

`version` is the version of your solution.

`doi` is the [Digital Object Identifier](https://www.doi.org/) (DOI)
for your solution.

`format_version` is the version of the HIPS format specification.

`timestamp` is the timestamp of the HIPS release or last update,
e.g. `2021-02-08T22:16:03.331998`.

`description` is the description of your HIPS.

`authors` is a string that can store multiple authors.

`cite` is the citation that should be used when the HIPS is used in a paper.

`git_repo` is the `https://gitlab.com/ida-mdc/hips-catalog`.

`tags` is a list of tags that should be associated with the HIPS,
    e.g. `["hips", "imaging"]`. This is represented as a Python list
    of Strings.
    
`license` is the name of the license that the HIPS is released under,
    e.g. "MIT".
    
`documentation` is a relative path to file with additional documentation in markdown.

`covers` is a list of images to be used as the cover for the HIPS on
        the catalog, where each image uses the asset representation.
        
Assets are represented as Python dictionaries like 
```python
{
        "source":
        "/assets/images/solutions/radial-symmetry/cover.png",
        "description":
        "Exemplary noisy input data and the resulting segmentation and point detection of radial symmetry."
}
```

`sample_inputs` is a list of assets using the asset representation.

`sample_outputs` is a list of assets using the asset representation.

`min_hips_version` is the minimum version of `hips` that is required for
    running this HIPS.
        
`tested_hips_version` is the version of `hips` that the solution was
    tested with.
    
`args` is a list of arguments, where arguments are specified as
    dictionaries of the form:
    
```python
{
        "name": "imp",
        "default":
        "./test_input.tif",
        "description": "Path to a 2D or 3D image stack",
        "action": lambda path: args.put("imp", IJ.openImage(path))
}
```

`install` is a Python function that is used to install/configure the
HIPS at install time. Note that `install` is run within the target
HIPS environment.
    
`init` is a Python function that is used to initalize the HIPS at
runtime.

`run` is a Python function that is used to run the HIPS.

`dependencies` is a Python dictionary that specifies the dependencies
for the HIPS.

# Examples

See:

- [Radial symmetry]({{ '/solutions/radial-symmetry.html' | prepend: site.baseurl | prepend: site.url }})
- [sciview volume rendering]({{
  '/solutions/sciview-volume-rendering.html' | prepend: site.baseurl | prepend: site.url }})
