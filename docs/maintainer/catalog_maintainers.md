---
layout: page
title: Maintaining a HIPS catalog
permalink: /docs/maintainer/catalog-maintainers/
---

Maintaining the HIPS catalog involves some curation that cannot be
automated. Catalog maintainers are curators of solutions and are
responsible for ensuring the security, stability, and reliability of
HIPS that are stored in the catalog. 

# Things to look out for

- Are the contributors authorized to update the solution?
- Does the solution reveal secret variables from the CI job?
