---
layout: page
title: Using a HIP Solution
permalink: /docs/user/using-hips/
---

* Using a HIP Solution

# 1. Why should i use hips?

There are 5 simple arguments:
- acknowledgement - You get mentioned every time your solution is executed.
- reproducibility - You ensure reproducibility due to a unique DOI exclusively for your solution.
- findability - People browse through the catalog and find your solution for their problem.
- accessibility - Your solution is easy accessible to everyone. 
- reliability - The hips framework ensures tested workability for your solution.


# 2. Install hips

## 2.1 Requirements:
- Installed anaconda python: https://www.anaconda.com

## 2.2 Installation routine:

To install hips perform the following steps:
```
# download the hips environment yaml
wget https://gitlab.com/ida-mdc/hips/-/raw/main/hips.yml

# create the hips conda environment with help of the yaml file
conda env create -f hips.yml

# activate the environment
conda activate hips

# install hips framwork in the environment 
pip install git+https://gitlab.com/ida-mdc/hips.git

```
That's it! Nothing more! try running `hips` to see whether it is properly installed.


## 2.3 Run a solution
Solutions are in general only python executable files. These files can make use of the hips framework to 
facilitate the sometimes rather complex installation routine of various tools. 

Solutions are stored and organized in catalogs who live outside of the hips framework. 
Currently, there is only one catalog https://gitlab.com/ida-mdc/hips-catalog storing all solution files.

To run a solution you only need to give the `solutionfile.py` to the hips python call. It looks like this:

```commandline
hips run path/to/solution.py
```   

When you run this, you should have a feedback telling you what is expected to proper
 execute the solution.
 
 ## 2.4 Deploy a solution
 You have come up with a nice solution (e.g. concrete example on how to treat certain input data with your tool)?
 Then you can write your own solution, deploy this to our catalog via a simple command. The rest is done via our hips
 framework. Simply run:
 
 ```commandline
hips deploy path/to/your/solution.py
``` 

You have an update for your solution? Just deploy again! The framework will do the rest for you!

But what happens inside the framework? Well, it is not complicated magic! This is what will happen:

- The installation routine specified in your solution is executed to check for dependency problems.
- A merge request to the catalog is created.
- The solution is registered at zenod (https://zenodo.org/) and a DOI is prereserved. 
- The solution is checked by the maintainer of the catalog if all coding standards and definitions are OK.
- Uppon merge request approval your solution is finally published to zenodo and the catalog is updated to include 
    the DOI for your solution.
- From now on you get mentioned every time your solution is run. Everyone can see you as author of the solution behind 
    your DOI. 

# 3. What will happen in the near future?
We currently work on extending the feature of our hips framework. In short, what we want to achieve in the near future:
- browsing through a catalog
- introducing `tutorials` as a way to (interactively) chain solutions to achieve a way to easily set up a specific solution 
relying on already published solutions - philosophy: Don't reinvent the wheel!
- Running solutions in a container.
- Remove installed solutions to free space.
- a way to obtain a pre-setup solution easily extendable to suit your needs.
- a nice documentation with tips and tricks.


