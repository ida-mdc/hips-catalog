---
layout: page
---
<div class="solutions">
    {% for solution in site.solutions %}
    <a class="solution" href="{{ solution.url  | relative_url }}" style="background-image: url('{{ solution.covers[0].source | relative_url }}')">
      <h2>{{ solution.name }}</h2>
      <div class="flex"></div>
      <div class="description">{{ solution.description | markdownify }}</div>
    </a>
    {% endfor %}
</div>