---
layout: page
title: Docs
permalink: /docs/
---

# Overall

[What are HIP Solutions?]({{ '/docs/overall/what-are-hips/' |
prepend: site.baseurl | prepend: site.url }})

# User

[Using a HIP Solution]({{ '/docs/user/using-hips/' | prepend: site.baseurl | prepend: site.url }})

# Maintainer

[Contributing a HIP Solution]({{ '/docs/maintainer/contributing-hips/' |
prepend: site.baseurl | prepend: site.url }})  
[Coding standards]({{ '/docs/maintainer/coding-standards/' | prepend:
site.baseurl | prepend: site.url}})  
[Catalog maintainers]({{ '/docs/maintainer/catalog-maintainers/' | prepend: site.baseurl | prepend: site.url}})  
