---
layout: page
title: About
permalink: /about/
---

HIP Solutions are developed as a part of the Helmholtz Imaging
Platform's (HIP) mission to bring scientists and engineers in the
Helmholtz Association together to promote and develop imaging science
and to foster synergies across imaging modalities and applications
within the Helmholtz Association.
